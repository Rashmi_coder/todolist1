import './App.css';

import React from "react";
import Table from './component/Table';
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Add from './component/Add';
function App() {
  
  return (
    <div className="App">
      <BrowserRouter>
<Routes>
<Route path='/' element={<Table/>}></Route>
  <Route path='/add' element={<Add/>}></Route>
  {/* <Route path='/update/:id' element={<Edit/>}></Route> */}
</Routes>
</BrowserRouter>
    </div>
  );
}

export default App;
