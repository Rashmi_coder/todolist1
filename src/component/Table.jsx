import React from 'react'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { FaEdit } from "react-icons/fa";
import { MdDelete } from "react-icons/md";
import {Link} from 'react-router-dom';
export default function Table() {
    const stockData = [
        {
          id:1,
          company: "Twitter Inc",
          ticker: "TWTR",
          stockPrice: "22.76 USD",
          timeElapsed: "5 sec ago",
        },
        {
          id:2,
          company: "Square Inc",
          ticker: "SQ",
          stockPrice: "45.28 USD",
          timeElapsed: "10 sec ago",
        },
        {
          id:3,
          company: "Shopify Inc",
          ticker: "SHOP",
          stockPrice: "341.79 USD",
          timeElapsed: "3 sec ago",
        },
        {
          id:4,
          company: "Sunrun Inc",
          ticker: "RUN",
          stockPrice: "9.87 USD",
          timeElapsed: "4 sec ago",
        },
        {
          id:5,
          company: "Adobe Inc",
          ticker: "ADBE",
          stockPrice: "300.99 USD",
          timeElapsed: "10 sec ago",
        },
        {
          id:6,
          company: "HubSpot Inc",
          ticker: "HUBS",
          stockPrice: "115.22 USD",
          timeElapsed: "12 sec ago",
        },
      ];
  return (
    <div>
      <div className="flex justify-center mt-10">
        <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
          <h1 className="text-3xl font-bold underline mb-10">
        TodoList
      </h1>
          <div className="inline-block min-w-full py-2 sm:px-6 lg:px-8">
          <Link to="/add" className="mt-10 mr-64 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">
  Add+
</Link>
            <div className="overflow-hidden mt-2">
              <DragDropContext>
              <table className="min-w-full text-center text-sm font-light">
                <thead
                  className="border-b bg-neutral-800 font-medium text-white dark:border-neutral-500 dark:bg-neutral-900">
                  <tr>
                    <th scope="col" className=" px-6 py-4">Company</th>
                    <th scope="col" className=" px-6 py-4">Ticker</th>
                    <th scope="col" className=" px-6 py-4">StockPrize</th>
                    <th scope="col" className=" px-6 py-4">Action</th>
                  </tr>
                </thead>
                <Droppable droppableId='tbody'>
                  {(provided)=>(
                <tbody ref={provided.innerRef}{...provided.droppableProps}>
                  {stockData.map((us,i)=>(
                  <Draggable draggableId={i}>
                  {(provided)=>(
                  <tr className="border-b dark:border-neutral-500" ref={provided.innerRef}{...provided.draggableProps}{...provided.dragHandleProps}>
                    <td className="whitespace-nowrap  px-6 py-4 font-medium">{us.company}</td>
                    <td className="whitespace-nowrap  px-6 py-4">{us.ticker}</td>
                    <td className="whitespace-nowrap  px-6 py-4">{us.stockPrice}</td>
                    <td className='flex mt-4'>
                        <FaEdit className='text-blue-800 mr-4'/>
<MdDelete className='text-red-700 text-lg'/>
                    </td>
                  </tr>
                  )}
                  </Draggable>
                  ))}
                </tbody>
                )}
                </Droppable>
              </table>
              </DragDropContext>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
